function squareSum(numbers){

    let output = 0

    for (let i = 0; i < numbers.length; ++i) {

        let math = numbers[i] * numbers[i]

        output += math

    }

    if (numbers.length === 0){
        return 0
    }
    return output
}