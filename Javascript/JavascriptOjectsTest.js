////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/* I didnt know where to even begin with this one so I looked up some help so what im going to do instead,
is create a racing version of this so I'm not just copy and pasting the code I will then leave the Kata for some
time and come back to it later and redo it */
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const RANKS = [ 
    "punk",
    "street kid",
    "commuter",
    "night driver",
    "boy racer",
    "Taxi driver",
    "Weaver",
    "street smart",
    "rally driver",
    "street racer",
    "drift king"
];

class Driver {
    constructor(){
        this._level = 1;
        this._experience = 100;
        this._achievements = [];
    }

    level() {
        // this.level = Math.floor(this._experience / 100);
        return Math.min(this._level, 100);
    }
    experience() {
        return Math.min(this._experience, 10000);
    }
    rank() {
        return RANKS[Math.floor(this._level / 10)] || RANKS[RANKS.length - 1];
    }
    achievements() {
        return this._achievements
    }
    training([description, exp, minLevel]) {
        if (minLevel > this._level) return "Not fast enough"
        this._achievements.push(description);
        this._experience += exp;
        this._level = Math.floor(this._experience / 100)
        return description;
    }
    isDriverSameRank(first, second) {
        return Math.floor(first / 10) === Math.floor(second / 10);
    }
    race(enemyLevel) {
        if (enemyLevel < 1 || enemyLevel > 100) {
            return "invalid level"
        }

        let raceResult = this.getRaceResult(enemyLevel);

        this._level = Math.floor(this._experience / 100)

        return raceResult;
    }

    getRaceResult(enemyLevel) {
        switch (true) {
            case this._level === enemyLevel:
                this._experience += 10;
                return "A good race"
            case this._level - enemyLevel === 1:
                this._experience += 5;
                return "A good race";
            case this._level - enemyLevel <= -5 && this.isDriverSameRank(this._level, enemyLevel):
                return "You have been beaten"
            case this._level < enemyLevel:
                this._experience += 20 * ((enemyLevel - this._level)*(enemyLevel - this._level));
                return "An intense race"
            default:
                return "Easy race"

        }

    }
}


let paul_walker = new Driver();
console.log(paul_walker.level())
console.log(paul_walker.experience())
console.log(paul_walker.rank())
console.log(paul_walker.achievements())
console.log(paul_walker.training(["Defeated Ludacris", 9000, 1]))
console.log(paul_walker.experience())
console.log(paul_walker.level())
console.log(paul_walker.rank())
console.log(paul_walker.race(90))
console.log(paul_walker.experience())
console.log(paul_walker.rank())
console.log(paul_walker.achievements())

  //https://www.codewars.com/kata/5941c545f5c394fef900000c/train/javascript

// let bruce_lee = new Warrior();
// bruce_lee.level();        // => 1
// bruce_lee.experience();   // => 100
// bruce_lee.rank();         // => "Pushover"
// bruce_lee.achievements(); // => []
// bruce_lee.training(["Defeated Chuck Norris", 9000, 1]); // => "Defeated Chuck Norris"
// bruce_lee.experience();   // => 9100
// bruce_lee.level();        // => 91
// bruce_lee.rank();         // => "Master"
// bruce_lee.battle(90);     // => "A good fight"
// bruce_lee.experience();   // => 9105
// bruce_lee.achievements(); // => ["Defeated Chuck Norris"]

//test

