export function binaryArrayToNumber(arr: number[]): number{

    let inputArray:number[] = arr

    let binaryArray: number[] = [1]
    
    let finalArray: number[] = []

    let finalValue: number = 0


    for (let i =0 ; i < arr.length-1; i++) {

        binaryArray.push(binaryArray[i]*2)

    }
    
    binaryArray.reverse()
    
    // console.log(binaryArray)

    for (let l = inputArray.length; l >= 0; l--) {
        
        if (inputArray[l] === 1){
            finalArray.push(binaryArray[l])
        }
    }
    // console.log(finalArray)

    for (let p = 0; p < finalArray.length; p++) {

        finalValue += finalArray[p]

    }
// console.log(finalValue)
return finalValue

}

binaryArrayToNumber([1,1,1,1])