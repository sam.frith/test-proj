function generateHashtag (str) {

    let output = ""
    let newString =str.split(" ")

    for (let i = 0; i < newString.length; i++) {
        newString[i] = newString[i].charAt(0).toUpperCase() + newString[i].slice(1)
    }

    output = "#" + newString.join(" ")
    output = output.replace(/\s/g, '')

    if (output == "#" ) {
        output = false 
        console.log(output)
        return output

    } else if (output.length > 140) {

            return output = false
    } else {
        
        console.log(output)
        return output
    }
}





//////////////////////////tests//////////////////////////////////////

generateHashtag("")

generateHashtag("Do We have A Hashtag")

generateHashtag("code" + " ".repeat(140) + "wars")

generateHashtag(" Hello there thanks for trying my Kata")
// // =>  "#HelloThereThanksForTryingMyKata"

generateHashtag("    Hello     World   ")                 
// // =>  "#HelloWorld"

generateHashtag("".repeat(200))                                        
// // =>  false

/////////////////////////////////////////////////////////////////////