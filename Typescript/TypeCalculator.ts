export function basicOp(operation: string, value1: number, value2: number): number {
    // Good luck!
    let output: number = 0
    
    if (operation  === "+") {
        output = value1 + value2

    } else if (operation  === "-") {
        output = value1 - value2
    
    } else if (operation  === "/") {
        output = value1 / value2

    } else if (operation  === "*") { 
        output = value1 * value2

    }
    return output
  }