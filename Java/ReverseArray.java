
public class ReverseArray {
    public static int[] digitize(long n) {
        // Code here
        String longString = String.valueOf(n);
        int[] array = new int[longString.length()];
    
        for (int i = 0; i < longString.length(); i++) {
            array[i] = Character.getNumericValue(longString.charAt(i));
        }
    
        // Reverse the array
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
    
        return array;
    }
}


