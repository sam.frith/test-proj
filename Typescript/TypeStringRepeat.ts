export function repeatStr(n: number, s: string): string {

    let output: string =""

    for (let i: number = 0; i <n; i++){

        output += s
    }

    return output
  }