function sortArray(array) {
    // Return a sorted array.
    let oddIndexes = [];
    let oddArray = [];
    let outputArray = array;

    function sortOddArray(array) {
        let output = array.sort(function(a,b){return a - b});
        // console.log(output);
        return output
    }
    // console.log(sortOddArray(array));

    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            // console.log(" character at index " + i + " is even so skipped");
        } else if (array[i] % 2 !== 0) {
            oddIndexes.push(i);
            oddArray.push(array[i]);
        }else {
            console.log("something went wrong")
        }
    // console.log(oddIndexes)
    }

    // console.log(oddIndexes)
    sortOddArray(oddArray)
    // console.log(oddArray)

    for (let i = 0; i < oddArray.length; i++) {
        outputArray.splice(oddIndexes[i], 1, oddArray[i]);
    }
    // console.log(outputArray)

    return outputArray


}









////////////////////////Tests//////////////////////////////////////////
//   [7, 1]  =>  [1, 7]
// [5, 8, 6, 3, 4]  =>  [3, 8, 6, 5, 4]
// [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]  =>  [1, 8, 3, 6, 5, 4, 7, 2, 9, 0]

sortArray([9, 8, 7, 6, 5, 4, 3, 2, 1, 0])
//////////////////////////////////////////////////////////////////