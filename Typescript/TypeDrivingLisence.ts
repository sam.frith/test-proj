export function driver(data: Array<string>): string{
    // Good luck
    let output: string= ""
    let firstNamePosition: number = 0
    let middleNamePosition: number = 1
    let lastNamePosition: number = 2
    let DOBPosition: number = 3
    let genderPosition: number = 4
    

    //1–5: The first five characters of the surname (padded with 9s if less than 5 characters)

    if (data[lastNamePosition].length < 5) {
        output += data[lastNamePosition].toUpperCase()
        while (output.length < 5){  
            output += 9
        }

    } else {
        output = data[lastNamePosition].substring(0, 5).toUpperCase()
    }

    //6: The decade digit from the year of birth (e.g. for 1987 it would be 8)

   for (let i = 0; i < data[DOBPosition].length; i++) {

    function hasNumber(myString: string) {
        return /\d/.test(myString);
      }
        if (data[DOBPosition][i] === '-' && hasNumber(data[DOBPosition][i+1]) ) {
            output += data[DOBPosition].charAt(i+3)
        }
   }


    //7–8: The month of birth (7th character incremented by 5 if driver is female i.e. 51–62 instead of 01–12)
    let month: string = ""

    for (let i = 0; i < data[DOBPosition].length; i++) {
        function isLetter(str: string) {
            return str.length === 1 && str.match(/[a-z]/i);
          }
            if (data[DOBPosition][i] === '-' && isLetter(data[DOBPosition][i+1])) {
                for (let j = 0; j < 3; j++) {
                month += data[DOBPosition][i+1+j].toUpperCase()
            }
        }
    }
    
    // console.log("first 3 of month are :", month)

    switch (month) {
        case (month = "JAN"):
            if (data[genderPosition] === "F" ) {
                output += "51"
            }else {
                output += "01"
            }
            break;
        case (month = "FEB"):
            if (data[genderPosition] === "F" ) {
                output += "52"
            }else {
                output += "02"
            }
            break;
        case (month = "MAR"):
            if (data[genderPosition] === "F" ) {
                output += "53"
            }else {
                output += "03"
            }
            break;
        case (month = "APR"):
            if (data[genderPosition] === "F" ) {
                output += "54"
            }else {
                output += "04"
            }
            break;
        case (month = "MAY"):
            if (data[genderPosition] === "F" ) {
                output += "55"
            }else {
                output += "05"
            }
            break;
        case (month = "JUN"):
            if (data[genderPosition] === "F" ) {
                output +="56"
            }else {
                output += "06"
            }
            break;
        case (month = "JUL"):
            if (data[genderPosition] === "F" ) {
                output += "57"
            }else {
                output +="07"
            }
            break;
        case (month = "AUG"):
            if (data[genderPosition] === "F" ) {
                output +="58"
            }else {
                output +="08"
            }
            break;
        case (month = "SEP"):
            if (data[genderPosition] === "F" ) {
                output += "59"
            }else {
                output += "09"
            }
            break;
        case (month = "OCT"):
            if (data[genderPosition] === "F" ) {
                output += "60"
            }else {
                output += "10"
            }
            break;
        case (month = "NOV"):
            if (data[genderPosition] === "F" ) {
                output += "61"
            }else {
                output += "11"
            }
            break;
        case (month = "DEC"):
            if (data[genderPosition] === "F" ) {
                output += "62"
            }else {
                output += "12"
            }
            break;
    }
    
    // console.log(output)


    // 9–10: The date within the month of birth
for (let i = 0; i < 2; i++) {
    output += data[DOBPosition][i]
}

    //11: The year digit from the year of birth (e.g. for 1987 it would be 7)

 output += data[DOBPosition][data[DOBPosition].length-1]

    //12–13: The first two initials of the first name and middle name, padded with a 9 if no middle name


    output += data[firstNamePosition][0]
    if (data[middleNamePosition].length === 0){
        output += "9"
    } else {
        output += data[middleNamePosition][0]
    }


    //14: Arbitrary digit – usually 9, but decremented to differentiate drivers with the first 13 characters in common. We will always use 9
    output += "9"

    //15–16: Two computer check digits. We will always use "AA"

    output += "AA"



////https://www.codewars.com/kata/586a1af1c66d18ad81000134/train/typescript

console.log(output)


    return output
  }

  driver(["John","James","Smith","01-Jan-2000","M"])

  driver(["Johanna","","Gibbs","13-Dec-1981","F"])

  driver(["Andrew","Robert","Lee","02-September-1981","M"])