function squareDigits(num){

    function square(n){
        let result = n*n
        return result
    }
    
    let output = ""

    let newArray = Array.from(num.toString()).map(Number);
    
    // console.log(newArray)

    for (let i = 0; i < newArray.length; i++) {
        let answer = square(newArray[i])
        // console.log(answer)
        output = output + answer;
    }
    
    output = parseInt(output)
    // console.log(output);
    return output;
  }

  squareDigits(9119)