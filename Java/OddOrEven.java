public class OddOrEven {
    public static String oddOrEven (int[] array) {

        String output = "";
        int total = 0;

        for (int i = 0; i < array.length; i++) {
            total += array[i];

        }

        if (total % 2 ==0){
            output = "even";
        } else {
            output = "odd";
        }

        return output;
        }
    
}
