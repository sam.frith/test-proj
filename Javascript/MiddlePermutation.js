function middlePermutation (s){

  var result = []

  let array = Array.from(s).sort();

  function factorialise(num) {
    if (num < 0) 
          return -1;
    else if (num == 0) 
        return 1;
    else {
        let output =(num * factorialise(num - 1));
        return output
        
    }
  }
  let factor = factorialise(array.length);

  // currentSize should be invoked with the array size
  function permutation(arr, currentSize) {
      if (currentSize === 1) { // recursion base-case (end)
          result.push(arr.join(""));
          return;
      }
      
      for (let i = 0; i < currentSize; i++){
          permutation(arr, currentSize-1);
          if (currentSize % 2 === 1) {
              let temp = arr[0];
              arr[0] = arr[currentSize - 1];
              arr[currentSize - 1] = temp;
          } else {
              let temp = arr[i];
              arr[i] = arr[currentSize - 1];
              arr[currentSize - 1] = temp;
          }
      }
  }
  

  permutation(array, array.length);

  console.log(result);
  
  let output = result[(factor/2)-1]

  console.log((factor/2)-1);

  console.log(output);


  return output;
  // use result here

}







// console.log(factorialize(s.length)/2)
// console.log(factorialize(s.length)/s.length);
// console.log(m/n)
  
  // middlePermutation("abc")// => "bac")
  // middlePermutation("abcd")// => "bdca")
  middlePermutation("abcdx")// => "cbxda")
  // middlePermutation("abcdxg")// =>"cxgdba")
  // middlePermutation("abcdxgz")// => "dczxgba")
