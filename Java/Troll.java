public class Troll {

    public static String disemvowel(String str) {
        // Code away...

        String[] vowels = {"a", "e", "i", "o", "u"};
        String output = "";

        for (int i = 0; i < str.length(); i++) {

            boolean isVowel = false;

            for (int j = 0; j < vowels.length; j++) {
            if (Character.toString(str.charAt(i)).toLowerCase().equals(vowels[j])) {
             isVowel = true;
             break;
            }            
        }
        if (isVowel == false ){
            output += str.charAt(i);
        }
    }
        return output;
} }
