function toCamelCase(str){

    const stringLength = str.length

    let baseString = str

    let newString = ''

    // console.log(newString)

    for (let i = 0; i < stringLength; i++) {

        if (baseString.charAt(i) === '-') {

            newString = newString + baseString.charAt(i+1).toUpperCase()
            i++ 

        } else if (baseString.charAt(i) === '_') {

            newString = newString + baseString.charAt(i+1).toUpperCase()
            i++ 

        } else {

            newString = newString + baseString.charAt(i)

        }
    }

    // console.log(newString)

    return newString
}

toCamelCase("the-stealth-warrior")

toCamelCase("The_Stealth_Warrior")


// "the-stealth-warrior" gets converted to "theStealthWarrior"


// "The_Stealth_Warrior" gets converted to "TheStealthWarrior"