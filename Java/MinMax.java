class MinMax {
    public static int[] minMax(int[] arr) {
    int min = arr[0];
    int max = arr[0];
    
    for (int i = 0; i < arr.length; i++) {
        
        if (arr[i] > max) {
            max = arr[i];
        } else if (arr[i] < min) {
            min = arr[i];
        }
    }
    
    // min & max added to output array, unsure how to do this in java.
    int[] output = {min, max};
    
    return output;
    }
}