
public class EvenorOdd {
    public static String even_or_odd(int number) {

        String output;

        if(number % 2 == 0)
          output = "Even";
        else
          output = "Odd";
      
          return output;
    }
}