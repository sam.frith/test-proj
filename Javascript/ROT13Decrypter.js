function rot13(str){
    //your code here
    const input = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM"
    const output = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" 
    let decoded = ""

    function IsLetter(l) {
        // console.log(l.toLowerCase() !=l.toUpperCase());
        return l.toLowerCase() !=l.toUpperCase() 
    }
    
    for (let i = 0; i < str.length; i++) {
        if (IsLetter(str.charAt(i)) == true ) {
            const index = input.indexOf(str[i])
            decoded += output[index]
        }else{
            decoded += str.charAt(i)
        }
    }
    
    console.log(decoded)
   return decoded
}

rot13("EBG13 rknzcyr.") // -> "ROT13 example."

rot13("This is my first ROT13 excercise!")// -> "Guvf vf zl svefg EBG13 rkprepvfr!"



