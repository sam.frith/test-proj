
//TODO: add level mechanism of every 100exp levelup occurs. 

// see JavascriptObjectTest for help  


class Warrior {
    constructor(name, achievements, training, battle) {
      this.name = name;
      this.level = '';
      this.experience = '';
      this.rank = '';
      this.achievements = achievements;
      this.training = training;
      this.battle = battle;
    }

    ///get//////////////////////////////////////////////////////////////////////
    get experience() {
        return this.experience;
    }



    /// set ////////////////////////////////////////////////////////////////////
    set experience(value) {
        const minValue= 100;
        const maxValue = 10000;

        if (value >= minValue && value <= maxValue) {
            this.experience = value;
          } else {
            console.error(`Invalid experience value: ${value}. Experience must be between ${minValue} and ${maxValue}.`);
        }
        switch (this.experience) {
            case (this.experience < 1000):
                this.rank = "Pushover"
                break;
            case (this.experience < 2000):
                this.rank = "Novice"
                break;
            case (this.experience < 3000):
                this.rank = "Fighter"
                break;
            case (this.experience < 4000):
                this.rank = "Warrior"
                break;
            case (this.experience < 5000):
                this.rank = "Vetren"
                break;
            case (this.experience < 6000):
                this.rank = "Sage"
                break;
            case (this.experience < 7000):
                this.rank = "Elite"
                break;
            case (this.experience < 8000):
                this.rank = "Conqueror"
                break;
            case (this.experience < 9000):
                this.rank = "Champion"
                break;
            case (this.experience < 10000):
                this.rank = "Master"
            case (this.experience = 10000):
                this.rank = "Greatest"
        

    }  }



    ////outputs////////////////////////////////////////////////////////////////
  
    status() {
      console.log(`${this.name} is a warrior at level ${this.level} `);
    }


  }

  const Rambo = new Warrior('Rambo', 5, 500);

  Rambo.status()





  //https://www.codewars.com/kata/5941c545f5c394fef900000c/train/javascript

// let bruce_lee = new Warrior();
// bruce_lee.level();        // => 1
// bruce_lee.experience();   // => 100
// bruce_lee.rank();         // => "Pushover"
// bruce_lee.achievements(); // => []
// bruce_lee.training(["Defeated Chuck Norris", 9000, 1]); // => "Defeated Chuck Norris"
// bruce_lee.experience();   // => 9100
// bruce_lee.level();        // => 91
// bruce_lee.rank();         // => "Master"
// bruce_lee.battle(90);     // => "A good fight"
// bruce_lee.experience();   // => 9105
// bruce_lee.achievements(); // => ["Defeated Chuck Norris"]

//test

