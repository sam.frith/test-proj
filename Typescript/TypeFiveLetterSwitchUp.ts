export function spinWords(words: string): string {
    //TODO Have fun :)

    let newArray: string[] = []

    let index: number = 0

    let output: string = "";

    function WordReverse(input: string) {
        
        let output: string = ""
    
        for (let j: number = input.length; j > 0 ;j--) { output+= input[j-1]}        
        return output
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    function Wordfinder(words: string) {
       let wordArray: string[] =[]
       let word: string = ""
        for (let i: number = 0; i < words.length; i++){
            if (words[i] != " "){ 
                word += words[i] 
            
            } else if( words[i] === " ") {
                wordArray.push(word)
                word = ""
            }
        }
        wordArray.push(word)
        // console.log(wordArray)
    return wordArray
}

////////////////////////////////////////////////////////////////////////////////////////////////

newArray= Wordfinder(words)
// console.log(newArray)

for (let i: number = 0; i < newArray.length; i++) {
    if (newArray[i].length >= 5) {
        newArray[i] = WordReverse(newArray[i])
    }
}
// console.log(newArray)

output = newArray.join(' ')
    
    console.log("final output: ", output)
    
    return output
    
}




////////////////////////////////////////////////////////////////

// spinWords("hello")

spinWords( "Hey fellow warriors" ) // => returns "Hey wollef sroirraw" 
spinWords( "This is a test") // => returns "This is a test" 
spinWords( "This is another test" ) // => returns "This is rehtona test"


//https://www.codewars.com/kata/5264d2b162488dc400000001/train/typescript


////////////////////////////////////////////////////////////////