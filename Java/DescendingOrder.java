import java.util.Arrays;

public class DescendingOrder {
    public static int sortDesc(final int num) {
      //Your code
      char[] digits = String.valueOf(num).toCharArray();

      Arrays.sort(digits);
      reverseArray(digits);
      

      int sortedNum = Integer.parseInt(new String(digits));

      return sortedNum;
    }

    private static void reverseArray(char[] arr) {
      for (int i = 0; i < arr.length /2; i++) {
        char temp = arr[i];
        arr[i] = arr[arr.length - 1 -i];
        arr[arr.length - 1 -i] = temp;
    }
  }
}
  
  
