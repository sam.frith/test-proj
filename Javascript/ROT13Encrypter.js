function rot13(message){
    //your code here
    const input = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" 
    const output = "nopqrstuvwxyzabcdefghijklmNOPQRSTUVWXYZABCDEFGHIJKLM"
    let encoded = ""

    function IsLetter(l) {
        // console.log(l.toLowerCase() !=l.toUpperCase());
        return l.toLowerCase() !=l.toUpperCase() 
    }
    
    for (let i = 0; i < message.length; i++) {
        if (IsLetter(message.charAt(i)) == true ) {
            const index = input.indexOf(message[i])
            encoded += output[index]
        }else{
            encoded += message.charAt(i)
        }
    }
    
    // console.log(encoded)
   return encoded
}

rot13("test")

rot13("Ruby is cool!")