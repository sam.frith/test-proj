
public class SamplePrint {
        public static void main(String[] args) {
        System.out.println("Hello Everyone this is on its own line");
        System.out.print("This on the otherhand, ");
        System.out.print(" Is all on the same line....");
        System.out.println("Now, time for some Maths:");
        System.out.println(5*5);
        System.out.println(7-4);

        String name = "John";
        System.out.println(name);

        int age = 26;
        System.out.println(age);

        int HeightCM;
        HeightCM = 185;
        System.out.println(HeightCM);

        int myNum = 5;
        float myFloatNum = 5.99f;
        char myLetter = 'D';
        boolean myBool = true;
        String myText = "Hello";

        System.out.println(myNum);
        System.out.println(myFloatNum);
        System.out.println(myLetter);
        System.out.println(myBool);
        System.out.println(myText);
        
        int x = 26;
        
        int y = 14;
        
        System.out.println(x + y);

        System.out.println("x = " + x);

        System.out.println("y = " + y);

        System.out.println("x + y = " + (x + y) );


        System.out.println("Employee name: " + name + "\nAge: " + age + "\nHeight: " + HeightCM );

        System.out.println("employee details repeated from a single variable");

        String employeeDetails = ("Employee name: " + name + "\nAge: " + age + "\nHeight: " + HeightCM );

        System.out.println(employeeDetails);

        int a = 5, b = 7, c = 9;

        System.out.println("sum of a + b + c  = " + (a + b + c));


        String textString = "This is an example of a string of text";

        System.out.println("The length of the textString variable is: " + textString.length() + " characters long!");

        System.out.println("This is textString in all upper case: " + textString.toUpperCase());

        System.out.println("The word 'example' is at the index " + textString.indexOf("example") + " in the string.");




        }
    
}
