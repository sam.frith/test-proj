public class PositiveOrNegative{

public static int[] countPositivesSumNegatives(int[] input){
    int count = 0;
    int sum = 0;
    int[] emptyArray = new int[0];
    
    if (input == null || input.length == 0) return emptyArray;

    for (int i = 0; i < input.length; i++){


       if (input[i] > 0) {
           count ++;
       } else if (input[i] < 0) {
        sum += input[i];
       }

    }
    int[] output = {count, sum};

 return output;
}}