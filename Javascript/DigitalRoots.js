function digitalRoot(n) {
        
    let newValues = Array.from(n.toString()).map(Number);

    let output = 0

    // console.log(newValues)
    
    function addValues(Values) {
        let root = 0
        for (i =0; i < Values.length; i++) {
            // console.log(newValues.at(i));
            root = root + Values.at(i)
            newValues = Array.from(root.toString()).map(Number);
        }
        return newValues
    }

    while (newValues.length > 1) {
        
        addValues(newValues)
        // console.log(newValues)
    }
    output = newValues.at(0)
    // console.log(output)

    return output

}

digitalRoot(155765)