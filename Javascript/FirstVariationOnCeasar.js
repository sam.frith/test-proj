function movingShift(s, shift) {
    const alphabet = 'abcdefghijklmnopqrstuvwxyz'
    const upperAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    let cipherText = ""

    for (let i = 0; i <s.length; i++) {

        let index
        const char = s[i]
        if (char === char.toUpperCase()) {
            index = upperAlphabet.indexOf(char)
        } else{
            index = alphabet.indexOf(char)
        }
        if (index === -1) {
            cipherText += char

        } else {
            const shiftedIndex = (index + shift) % alphabet.length
            if (char === char.toUpperCase()) {
                cipherText += upperAlphabet[shiftedIndex]
            } else {
                cipherText += alphabet[shiftedIndex]
            }
        }
        shift++
        
    // console.log(cipherText)
}


// console.log("cipherText.length = ",cipherText.length,", division = ", division, ", remainder = ", remainder)

function splitMessage(message) {
    const messageLength = message.length
    const partLength = Math.ceil(messageLength / 5)
    let remainingLength = messageLength
    const parts = []


    let startingPoint = 0
    let finishingPoint = partLength
        for (let i = 0; i < 4; i++) {
        const part = message.slice(startingPoint, finishingPoint); // get the next part of the message
        parts.push(part);
        remainingLength -= partLength
        startingPoint += partLength
        finishingPoint += partLength
        }

    if (remainingLength > 0) {
      const lastPartLength = remainingLength <= partLength ? remainingLength : partLength - 1
      const lastPart = message.slice(messageLength - lastPartLength, messageLength)
      parts.push(lastPart)
    } else {
      parts.push('')
    }
    console.log(parts)
    return parts
  }
return splitMessage(cipherText)
}








function demovingShift(arr, shift) {

    const alphabet = 'abcdefghijklmnopqrstuvwxyz'
    const upperAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    let cipherText = arr.join("")
    let plainText = ""

    console.log(cipherText)

    for (let i = 0; i <cipherText.length; i++) {

        let index
        const char = cipherText[i]
        if (char === char.toUpperCase()) {
            index = upperAlphabet.indexOf(char)
        } else{
            index = alphabet.indexOf(char)
        }
        if (index === -1) {
            plainText += char

        } else {
            let shiftedIndex = (index - shift) % alphabet.length
            let value = shiftedIndex
            console.log("shifted index value: " + shiftedIndex, "value, value: " + value)
            console.log("alphabet.length = ", alphabet.length)
            if (shiftedIndex < 0){
                value = value * -1
                console.log(" shifted index is less than zero ")
                shiftedIndex = 26 - value
            }
            console.log("updated shifted index value: ", shiftedIndex)

            if (char === char.toUpperCase()) {
                plainText += upperAlphabet[shiftedIndex]
            } else {
                plainText += alphabet[shiftedIndex]
            }
        }
        shift++
        
    }
    console.log(plainText)




    return plainText;
}









let u = "I should have known that you would have a perfect answer for me!!!"
let v = [
    'J vltasl rlhr ',
    'zdfog odxr ypw',
    ' atasl rlhr p ',
    'gwkzzyq zntyhv',
    ' lvz wp!!!'
  ]

// movingShift(u, 1)

demovingShift(v, 1)