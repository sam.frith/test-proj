var uniqueInOrder=function(iterable){

    let newArray = [];

    console.log('iterable length: '+iterable.length)

    for (let i = 0; i<iterable.length; i++) {
        
        if (iterable.at(i) === iterable.at(i + 1)) {
        } else {

            newArray.push(iterable.at(i));
        }
        // console.log(newArray)
    }
    // console.log(newArray)
    return newArray
}

uniqueInOrder('AAAABBBCCDAABBB')
// == ['A', 'B', 'C', 'D', 'A', 'B']

// uniqueInOrder('ABBCcAD') 
// == ['A', 'B', 'C', 'c', 'A', 'D']

// uniqueInOrder([1,2,2,3,3])      
// == [1,2,3]