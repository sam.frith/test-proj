export function multiplicationTable (size: number): number[][] {
  // Implement me! :)
  let output: number[][] = [];

  if (size === 0) {
    // console.log("size = 0, exiting")
    return output;
  } else{
    // console.log('inside the if - else ')
    for (let i = 0; i < size; i++){
    let newArray: number[] = []
    let j:number = 1
    let startingNumber:number = i+1
    // console.log("hit while for loop")
    while (j < size+1){
        newArray.push(startingNumber * j)
        j++;
        // console.log("hit while loop")
    }
    // console.log("newArray = ", newArray)
    output.push(newArray)
    }

  }
console.log("output =", output)
return output
}

// multiplicationTable(1)
multiplicationTable(5)