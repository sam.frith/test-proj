

public class CarKata {
        public static int rentalCarCost(int d) {
          int dailyCost = 40;
          int totalCost = dailyCost * d;

          if (d >= 7){
            totalCost -= 50;
          } else if (d >= 3){
            totalCost -= 20;
          }


          return totalCost;
        }
      }