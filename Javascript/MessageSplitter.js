

function splitMessage(message) {
    const messageLength = message.length;
    const partLength = Math.ceil(messageLength / 5); // calculate the target length for each part
    let remainingLength = messageLength;
    const parts = [];


    let startingPoint = 0
    let finishingPoint = partLength
        for (let i = 0; i < 4; i++) {
        const part = message.slice(startingPoint, finishingPoint); // get the next part of the message
        parts.push(part);
        remainingLength -= partLength;
        startingPoint += partLength; 
        finishingPoint += partLength
        }

    if (remainingLength > 0) {
      const lastPartLength = remainingLength <= partLength ? remainingLength : partLength - 1;
      const lastPart = message.slice(messageLength - lastPartLength, messageLength);
      parts.push(lastPart);
    } else {
      parts.push('');
    }
    console.log(parts)
    return parts;
  }
    
    


splitMessage("I should have known that you would have a perfect answer for me!!!")