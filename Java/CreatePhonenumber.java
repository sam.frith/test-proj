public class CreatePhonenumber {
    
    public static String createPhoneNumber(int[] numbers) {
        
       StringBuilder sb = new StringBuilder();
       sb.append("(");

       for (int i = 0; i < 3; i++) {
       sb.append(numbers[i]);
       }
       sb.append(") ");

       for (int i = 3; i < 6; i++) {
        sb.append(numbers[i]);
       }

       sb.append("-");
       for (int i = 6; i < 10; i++) {
        sb.append(numbers[i]);
       }

       String output = sb.toString();
       return output;
      }
    }
