function moveZeros(arr) {

    let newArray = []
    let zeroArray = []

    for (let i = 0; i < arr.length; i++) {

        if (arr[i] === 0 || arr[i] === +0) {

            zeroArray.push(arr[i]);
            
        } else {
            newArray.push(arr[i]);
        }
    }
    for (let n = 0; n < zeroArray.length; n++) {
        newArray.push(zeroArray[n]);
    }

    console.log(newArray)
    return newArray
}





  ////////////////////////tests/////////////////////////

//   moveZeros([false,1,0,1,2,0,1,3,"a"]) 
  // returns[false,1,1,2,1,3,"a",0,0]

//   moveZeros([ '5', '9', 7, 3, 4, {}, '7', '8', [], null, 7, '6', [], 4, 5, null, '6', 3, true, '9', 7, '0', '0' ])

//   moveZeros([ 9, +0, 9, 1, 2, 1, 1, 3, 1, 9, +0, +0, 9, +0, +0, +0, +0, +0, +0, +0 ])

//   moveZeros([
//     9, 9, 1, 2, 1, 1, 3,
//     1, 9, 0, 9, 0, 0, 0,
//     0, 0, 0, 0, 0, 0
//   ])

  ////////////////////////tests/////////////////////////