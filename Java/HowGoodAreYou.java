public class HowGoodAreYou {
    public static boolean betterThanAverage(int[] classPoints, int yourPoints) {

        boolean output = false;

        int total = 0;

        for (int i = 0; i < classPoints.length; i++){
            total += classPoints[i];
        }
        
        if (yourPoints > (total/classPoints.length)){
            output = true;

        }
       
        return output;
    }
}
