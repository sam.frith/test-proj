function goodVsEvil(good, evil){
  
console.log("goog input = " + good, "evil input = " + evil);
let goodArray = good.split(" ");
let evilArray = evil.split(" ");

let hobbits = 1
let orcs = 1
let men = 2
let wargs = 2
let goblins = 2
let elves = 3
let dwarves = 3
let urukHai = 3
let eagles = 4
let trolls = 5
let wizards = 10


function GoodCalculation(goodArray){
    let goodTotal = 0
    
    goodTotal += (goodArray[0]*hobbits)
    goodTotal += (goodArray[1]*men)

    goodTotal += (goodArray[2]*elves)
    goodTotal += (goodArray[3]*dwarves)

    goodTotal += (goodArray[4]*eagles)
    goodTotal += (goodArray[5]*wizards)
    return goodTotal
}

function EvilCalculation(evilArray){
    let evilTotal = 0
    
    evilTotal += (evilArray[0]*orcs)
    evilTotal += (evilArray[1]*men)

    evilTotal += (evilArray[2]*wargs)
    evilTotal += (evilArray[3]*goblins)

    evilTotal += (evilArray[4]*urukHai)
    evilTotal += (evilArray[5]*trolls)

    evilTotal += (evilArray[6]*wizards)
    return evilTotal
}


let goodTotal = GoodCalculation(goodArray)
let evilTotal = EvilCalculation(evilArray)
let output = ""
  
if (goodTotal > evilTotal ){
    output = 'Battle Result: Good triumphs over Evil' 
} else if (goodTotal < evilTotal ){
    output = 'Battle Result: Evil eradicates all trace of Good'
} else if (goodTotal === evilTotal ){
    output = 'Battle Result: No victor on this battle field'
}

console.log("goodTotal: " + goodTotal, "evilTotal: " + evilTotal)

console.log(output)
return output
}


goodVsEvil('1 1 1 1 1 1', '1 1 1 1 1 1 1')