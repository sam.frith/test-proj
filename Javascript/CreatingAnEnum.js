let stash = ["hero", "can", "save", "us"]


function createNewEnum(values) {
      let enumObject = {};
      values.forEach((value, index) => {
        enumObject[value] = index;
      });
      return Object.freeze(enumObject);
    }

  const newStash = createNewEnum(stash);

  console.log(newStash);

