class FirstNonConsecutive {
    static Integer find(final int[] array) {

        for (int i = 1; i < array.length; i++) {

            int output = 0;
            

            if (array[i] - array[i-1] != 1){
                output = array[i];

                return output;
            } 

        }
        return null;
    }
}
