export function solution(number: number): string {
    // convert the number to a roman numeral
        
        
        // console.log(number)
        
        let inputLength: number = number.toString().length
        
    
        // console.log(inputLength)
    
        let inputString: string = number.toString()
    
        // console.log(inputString)
    
        let romanAnswer: string = ''
    
    
    
    function ThousandsDigit(string: string): string {
        let thousandOutput = ""
        switch (string) {
            case "0":
                thousandOutput = "";
            case "1":
                thousandOutput = "M";
                break;
            case "2":
                thousandOutput = "MM";
                break;
            case "3":
                thousandOutput = "MMM";
                break;
            }
            return thousandOutput;
    }
    
    function HundredsDigit(string: string): string {
        let hundredsOutput = ""
        switch (string) {
            case "0":
                hundredsOutput = "";
                break;
            case "1":
                hundredsOutput = "C";
                break;
            case "2":
                hundredsOutput = "CC";
                break;
            case "3":
                hundredsOutput = "CCC";
                break;
            case "4":
                hundredsOutput = "CD";
                break;
            case "5":
                hundredsOutput = "D";
                break;
            case "6":
                hundredsOutput = "DC";
                break;
            case "7":
                hundredsOutput = "DCC";
                break;
            case "8":
                hundredsOutput = "DCCC";
                break;
            case "9":
                hundredsOutput = "CM";
                break;
            }
            return hundredsOutput;
    }
    
    function TensDigit(string: string): string {
        let hundredsOutput = ""
        switch (string) {
            case "0":
                hundredsOutput = "";
                break;
            case "1":
                hundredsOutput = "X";
                break;
            case "2":
                hundredsOutput = "XX";
                break;
            case "3":
                hundredsOutput = "XXX";
                break;
            case "4":
                hundredsOutput = "XL";
                break;
            case "5":
                hundredsOutput = "L";
                break;
            case "6":
                hundredsOutput = "LX";
                break;
            case "7":
                hundredsOutput = "LXX";
                break;
            case "8":
                hundredsOutput = "LXXX";
                break;
            case "9":
                hundredsOutput = "XC";
                break;
            }
            return hundredsOutput;
    }
    
    function UnitsDigit(string: string): string{
        let unitsOutput = ""
        switch (string) {
            case "0":
                unitsOutput = "";
                break;
            case "1":
                unitsOutput = "I";
                break;
            case "2":
                unitsOutput = "II";
                break;
            case "3":
                unitsOutput = "III";
                break;
            case "4":
                unitsOutput = "IV";
                break;
            case "5":
                unitsOutput = "V";
                break;
            case "6":
                unitsOutput = "VI";
                break;
            case "7":
                unitsOutput = "VII";
                break;
            case "8":
                unitsOutput = "VIII";
                break;
            case "9":
                unitsOutput = "IX";
                break;
            }
            return unitsOutput;
    }
    
    
    if (inputLength == 4) {
        romanAnswer = ThousandsDigit(inputString[0])
        // console.log(romanAnswer)
        romanAnswer = romanAnswer + HundredsDigit(inputString[1])
        // console.log(romanAnswer)
        romanAnswer = romanAnswer + TensDigit(inputString[2])
        // console.log(romanAnswer)
        romanAnswer = romanAnswer + UnitsDigit(inputString[3])
        console.log(romanAnswer)
    
    } else if (inputLength == 3) {
        romanAnswer = romanAnswer + HundredsDigit(inputString[0])
        // console.log(romanAnswer)
        romanAnswer = romanAnswer + TensDigit(inputString[1])
        // console.log(romanAnswer)
        romanAnswer = romanAnswer + UnitsDigit(inputString[2])
        console.log(romanAnswer)
    
    } else if (inputLength == 2) {
        romanAnswer = romanAnswer + TensDigit(inputString[0])
        // console.log(romanAnswer)
        romanAnswer = romanAnswer + UnitsDigit(inputString[1])
        console.log(romanAnswer)
    
    } else if (inputLength == 1) {
        romanAnswer = romanAnswer + UnitsDigit(inputString[0])
        console.log(romanAnswer)
    
    } else {
        console.log("not a 4 digit number")
    }
    
    return romanAnswer
    
    }
    
    
    //Thousands tests
    
    // solution (1000)
    // solution (2000)
    // solution (3000)
    
    //hundreds tests
    
    // solution (1100)
    // solution (1200)
    // solution (1300)
    // solution (1400)
    // solution (1500)
    // solution (1600)
    // solution (1700)
    // solution (1800)
    // solution (1900)
    
    //Tens tests
    
    // solution (1110)
    // solution (1120)
    // solution (1130)
    // solution (1140)
    // solution (1150)
    // solution (1160)
    // solution (1170)
    // solution (1180)
    // solution (1190)
    
    //Units tests
    
    // solution (1111)
    // solution (1112)
    // solution (1113)
    // solution (1114)
    // solution (1115)
    // solution (1116)
    // solution (1117)
    // solution (1118)
    // solution (1119)
    
    solution(2704)





