export function bmi(weight: number, height: number): string {
  // TODO: implement this method
let BMI: number = weight/(height*height);

if (BMI <= 18.5) {
    return "Underweight";
}else if (BMI <= 25.0) {
    return "Normal";
} else if (BMI <= 30.0) {
    return "Overweight";
} else if (BMI > 30) {
    return "Obese"
}else {

    throw new Error("The method or operation is not implemented.");
}

}